package pkemonyc.util;

import lombok.Data;
import java.util.List;

/**
 * 蛋类
 * @Classname Egg
 * @Date 2025/2/10 23:21
 * @Created by 87766867@qq.com
 */
@Data
public class Egg {
    Pokemon baby;           // 蛋中的宝可梦
    int remainingSteps;     // 剩余孵化步数
    List<String> moves;     // 遗传的招式

    public Egg(Pokemon baby, int steps, List<String> moves) {
        this.baby = baby;
        this.remainingSteps = steps;
        this.moves = moves;
    }
}
