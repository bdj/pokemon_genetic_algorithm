package pkemonyc.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StartMain {


	public static void main(String[] args) {

		// 初始化喷火龙（雄性）
		Pokemon charizard = new Pokemon(
				"喷火龙",
				new Pokemon.EggGroup[]{Pokemon.EggGroup.MONSTER, Pokemon.EggGroup.DRAGON},
				Pokemon.Gender.MALE,
				new int[]{31, 30, 25, 20, 28, 31},
				false,
				false,
				"ENG",
				Pokemon.BallType.MASTER_BALL,
				Pokemon.Nature.ADAMANT,
				new IncubationItems(false,false,false,false,false)
		);
		charizard.eggMoves = Arrays.asList("龙之舞", "逆鳞");

		System.out.println("=== 第一个宝可梦蛋生成成功 ===");
		System.out.println("物种: " + charizard.species);
		System.out.println("个体值: " + Arrays.toString(charizard.IVs));
		System.out.println("性格: " + charizard.nature);System.out.println("性别: "+ charizard.gender);
		System.out.println("隐藏特性: "+ (charizard.hasHiddenAbility? "是":"否"));
		System.out.println("闪光: " + (charizard.isShiny ? "✨" : "普通"));

		//雌性
		Pokemon ditto = new Pokemon(
				"喷火龙",
				new Pokemon.EggGroup[]{Pokemon.EggGroup.MONSTER, Pokemon.EggGroup.DRAGON},
				Pokemon.Gender.FEMALE,
				new int[]{31, 31, 31,31, 31, 31},
				true,
				false,
				"chin",
				Pokemon.BallType.MASTER_BALL,
				Pokemon.Nature.BOLD,
				new IncubationItems(true,true,true,true,true)
		);

		System.out.println("=== 第二个宝可梦蛋生成成功 ===");
		System.out.println("物种: " + ditto.species);
		System.out.println("个体值: " + Arrays.toString(ditto.IVs));
		System.out.println("性格: " + ditto.nature);
		System.out.println("性别: "+ charizard.gender);
		System.out.println("隐藏特性: "+ (charizard.hasHiddenAbility? "是":"否"));
		System.out.println("闪光: " + (ditto.isShiny ? "✨" : "普通"));

		// 初始化百变怪
//        Pokemon ditto = new Pokemon(
//                "Ditto",
//                new Pokemon.EggGroup[]{Pokemon.EggGroup.DITTO},
//                Pokemon.Gender.GENDERLESS,
//                new int[]{31, 31, 31, 31, 31, 31},
//                true,
//                "JPN",
//                Pokemon.BallType.POKE_BALL,
//                Pokemon.Nature.CALM
//        );


		List<Egg> eggs = new ArrayList<>();
		//执行10次
		for (int i = 0; i < 1; i++) {

			// 执行孵蛋
			Incubator incubator = new Incubator();
			Egg egg = incubator.generateEgg(charizard, ditto);

			// 输出结果
			if (egg != null) {
				System.out.println("=== 宝可梦蛋生成成功 ===");
				System.out.println("物种: " + egg.baby.species);
				System.out.println("个体值: " + Arrays.toString(egg.baby.IVs));
				System.out.println("性格: " + egg.baby.nature);
				System.out.println("性别: "+ egg.baby.gender);
				System.out.println("隐藏特性: "+ (egg.baby.hasHiddenAbility? "是":"否"));
				System.out.println("闪光: " + (egg.baby.isShiny ? "✨" : "普通"));
				System.out.println("所需步数: " + egg.remainingSteps);
				if(egg.baby.isShiny){
					System.out.println("ssssssss");
					eggs.add(egg);
				}
			} else {
				System.out.println("无法生成蛋");
			}
		}
		for (Egg egg : eggs) {
			System.out.println("=== 闪光宝可梦蛋生成成功 ===");
			System.out.println("物种: " + egg.baby.species);
			System.out.println("个体值: " + Arrays.toString(egg.baby.IVs));
			System.out.println("性格: " + egg.baby.nature);
			System.out.println("性别: "+ egg.baby.gender);
			System.out.println("隐藏特性: "+ (egg.baby.hasHiddenAbility? "是":"否"));
			System.out.println("闪光: " + (egg.baby.isShiny ? "✨" : "普通"));
			System.out.println("所需步数: " + egg.remainingSteps);
		}

	}
}
