package pkemonyc.util;

import lombok.Data;

/**
 * 孵蛋道具持有类
 * @Classname IncubationItems
 * @Date 2025/2/10 23:20
 * @Created by 87766867@qq.com
 */
@Data
public class IncubationItems {
    boolean destinyKnot;    // 红线（遗传5项个体值）
    boolean everstone;      // 不变石（锁定性格）
    boolean powerItem;      // 力量道具（单项个体值遗传）
    boolean shinyCharm;     // 闪耀护符（提升闪光概率）
    boolean incense;        // 熏香（影响孵化形态）

    /**
     *
     * @param destinyKnot 红线
     * @param everstone 不变石
     * @param powerItem 力量道具（单项个体值遗传）
     * @param shinyCharm  闪耀护符（提升闪光概率）
     * @param incense 熏香（影响孵化形态）
     */
    public IncubationItems(boolean destinyKnot, boolean everstone, boolean powerItem, boolean shinyCharm, boolean incense) {
        this.destinyKnot = destinyKnot;
        this.everstone = everstone;
        this.powerItem = powerItem;
        this.shinyCharm = shinyCharm;
        this.incense = incense;
    }
}
