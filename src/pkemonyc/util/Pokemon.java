package pkemonyc.util;

import lombok.Data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 宝可梦个体类
 */
@Data
public class Pokemon {
    // 基础信息
    String species;         // 物种名称
    EggGroup[] eggGroups;   // 所属蛋组（最多2个）
    boolean isDitto;        // 是否为百变怪
    Gender gender;          // 性别
    String language;        // 来源语言（用于国际孵蛋）

    // 遗传属性
    int[] IVs = new int[6]; // 个体值数组（HP, 攻击, 防御, 特攻, 特防, 速度）
    boolean hasHiddenAbility;// 是否拥有隐藏特性
    Nature nature;          // 性格
    BallType ballType;      // 球种
    boolean isShiny;        // 是否为闪光
    // 形态相关
    String form;            // 形态标识（如"Alolan"）
     List<String> eggMoves;  // 可遗传的蛋招式


    IncubationItems heldItems; // 新增持有道具属性


    /**
     * 构造方法
     * @param species  物种名称
     * @param eggGroups 蛋组（最多2个）
     * @param gender 性别
     * @param IVs 个体值数组（HP, 攻击, 防御, 特攻, 特防, 速度）
     * @param hasHiddenAbility 是否拥有隐藏特性
     * @param isDitto 是否为百变怪
     * @param language 语言（用于国际孵蛋）国籍
     * @param ballType 球种
     * @param nature  性格
     * @param heldItems 道具属性
     */
    public Pokemon(String species, EggGroup[] eggGroups, Gender gender,
                   int[] IVs,boolean hasHiddenAbility, boolean isDitto, String language, BallType ballType,Nature nature,IncubationItems heldItems) {
        this.species = species;
        this.eggGroups = Arrays.copyOf(eggGroups, eggGroups.length);
        this.gender = gender;
        this.IVs = Arrays.copyOf(IVs, 6);
        this.hasHiddenAbility=hasHiddenAbility;
        this.isDitto = isDitto;
        this.language = language;
        this.ballType = ballType;
        this.eggMoves = new ArrayList<>();
        this.nature=nature;
        this.heldItems=heldItems;
    }

    /**
     * 蛋组枚举（包含全部官方蛋组）
     */
    public enum EggGroup {
        MONSTER,     // 怪兽组：包含外形类似怪兽的宝可梦（如：小火龙、卡比兽）
        WATER1,      // 水中1组：包含水生或两栖类宝可梦（如：杰尼龟、鲤鱼王）
        WATER2,      // 水中2组：包含大型水生生物宝可梦（如：暴鲤龙、吼鲸王）
        WATER3,      // 水中3组：包含无脊椎水生生物宝可梦（如：墨海马、海星星）
        BUG,         // 虫组：包含昆虫类宝可梦（如：绿毛虫、凯罗斯）
        FLYING,      // 飞行组：包含鸟类或飞行类宝可梦（如：波波、大比鸟）

        FIELD,       // 陆上组：包含常见的陆地生物宝可梦（如：小拉达、伊布）
        FAIRY,       // 妖精组：包含妖精类宝可梦（如：皮皮、波克比）
        GRASS,       // 植物组：包含植物类宝可梦（如：妙蛙种子、走路草）
        HUMAN_LIKE,  // 人形组：包含类人型宝可梦（如：豪力、沙奈朵）
        MINERAL,     // 矿物组：包含矿物或岩石类宝可梦（如：小拳石、可可多拉）
        AMORPHOUS,   // 不定形组：包含无固定形态的宝可梦（如：鬼斯、臭泥）

        DITTO,       // 百变怪组：仅包含百变怪，可与任何非未发现组的宝可梦孵蛋
        DRAGON,      // 龙组：包含龙类宝可梦（如：迷你龙、宝贝龙）
        UNDISCOVERED // 未发现组：无法通过孵蛋获得的宝可梦（如：神兽、幻兽）
    }

    /**
     * 性别枚举（包含无性别状态）
     */
    public enum Gender {
        MALE,        // 雄性
        FEMALE,      // 雌性
        GENDERLESS   // 无性别
    }

    /**
     * 性格枚举（包含全部25种性格）
     */
    /**
     * 性格枚举类
     * 包含全部25种性格，每种性格会提升和降低特定能力
     */
    public  enum Nature {
        HARDY,    // 勤奋：无能力修正
        LONELY,   // 怕寂寞：攻击↑ 防御↓
        BRAVE,    // 勇敢：攻击↑ 速度↓
        ADAMANT,  // 固执：攻击↑ 特攻↓
        NAUGHTY,  // 顽皮：攻击↑ 特防↓

        BOLD,     // 大胆：防御↑ 攻击↓
        DOCILE,   // 坦率：无能力修正
        RELAXED,  // 悠闲：防御↑ 速度↓
        IMPISH,   // 淘气：防御↑ 特攻↓
        LAX,      // 乐天：防御↑ 特防↓

        TIMID,    // 胆小：速度↑ 攻击↓
        HASTY,    // 急躁：速度↑ 防御↓
        SERIOUS,  // 认真：无能力修正
        JOLLY,    // 爽朗：速度↑ 特攻↓
        NAIVE,    // 天真：速度↑ 特防↓

        MODEST,   // 内敛：特攻↑ 攻击↓
        MILD,     // 慢吞吞：特攻↑ 防御↓
        QUIET,    // 冷静：特攻↑ 速度↓
        BASHFUL,  // 害羞：无能力修正
        RASH,     // 马虎：特攻↑ 特防↓

        CALM,     // 温和：特防↑ 攻击↓
        GENTLE,   // 温顺：特防↑ 防御↓
        SASSY,    // 自大：特防↑ 速度↓
        CAREFUL,  // 慎重：特防↑ 特攻↓
        QUIRKY;   // 浮躁：无能力修正
    }


    /**
     * 精灵球类型枚举（包含主要球种）
     */
  public  enum BallType {
        POKE_BALL,    // 普通球
        GREAT_BALL,   // 超级球
        ULTRA_BALL,   // 高级球
        MASTER_BALL,  // 大师球
        DUSK_BALL,    // 黑暗球
        HEAL_BALL     // 治愈球
    }
}

